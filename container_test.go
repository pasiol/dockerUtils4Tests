package dockerUtils4Tests

import "testing"

func TestStartContainer(t *testing.T) {

	image := "mongo:latest"
	name := "containertest"
	_, _ = StopContainer(name)
	err := StartContainer(image, name, 27017, true)
	if err != nil {
		t.Logf("error should be nil: %s", err)
		t.Fail()
	}
}

func TestStopContainer(t *testing.T) {
	image := "mongo:latest"
	name := "containertest"
	_ = StartContainer(image, name, 27017, true)
	_, err := StopContainer(name)
	if err != nil {
		t.Logf("error should be nil: %s", err)
		t.Fail()
	}
}

func TestStopContainerNotExists(t *testing.T) {
	name := "containertest"
	_, err := StopContainer(name)
	if err != nil && err.Error() != "Error response from daemon: page not found" {
		t.Logf("error should be nil: %s", err)
		t.Fail()
	}
}
