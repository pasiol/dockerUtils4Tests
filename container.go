package dockerUtils4Tests

import (
	"context"
	"strconv"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
	v1 "github.com/opencontainers/image-spec/specs-go/v1"
)

func getAllContainers() ([]types.Container, error) {
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		return []types.Container{}, err
	}

	containers, err := cli.ContainerList(context.Background(), types.ContainerListOptions{})
	if err != nil {
		return []types.Container{}, err
	}

	return containers, nil
}

func findContainer(name string) (string, error) {
	containers, err := getAllContainers()
	if err != nil {
		return "", err
	}
	for _, container := range containers {
		for _, n := range container.Names {
			if name == n[1:] {
				return container.ID, nil
			}
		}
	}
	return "", nil

}

func StartContainer(image string, name string, port int, autoRemove bool) error {
	cli, err := client.NewClientWithOpts()
	if err != nil {
		return err
	}
	ctx := context.Background()
	p := nat.Port(strconv.Itoa(port))
	resp, err := cli.ContainerCreate(ctx, &container.Config{
		Image:        image,
		ExposedPorts: nat.PortSet{p: struct{}{}},
	}, &container.HostConfig{
		PortBindings: map[nat.Port][]nat.PortBinding{nat.Port(strconv.Itoa(port)): {{HostIP: "127.0.0.1", HostPort: strconv.Itoa(port)}}}, AutoRemove: autoRemove,
	}, nil, &v1.Platform{Architecture: "amd64", OS: "linux"}, name)
	if err != nil {
		return err
	}

	if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		return err
	}
	return nil

}

func StopContainer(name string) (string, error) {
	id, err := findContainer(name)
	if err != nil {
		return "", err
	}
	cli, err := client.NewClientWithOpts()
	if err != nil {
		return "", err
	}
	ctx := context.Background()
	if err := cli.ContainerStop(ctx, id, nil); err != nil {
		return "", err
	}
	return id, nil
}
